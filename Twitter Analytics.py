#!/usr/bin/env python
# coding: utf-8

# In[11]:


API_KEY='P4opscCYayfeU7OQFnkqh7WYQ'
API_SECRET_KEY='g2pi4dzjfOYYSoxWyZWHmtlSjUicAizfaKiVM20zvAOQJ8eNo1'
ACCESS_TOKEN='1167359584446513152-HTnVMVfwS0AaJyMLGvmA4genAT2ewO'
ACCESS_TOKEN_SECRET='fnC3UhDvrDox9XGT6pPivRQYk42Wpt8XK7V3VUrzllAFQ'


# In[12]:


import tweepy
import json

auth = tweepy.OAuthHandler(API_KEY, API_SECRET_KEY)
auth.set_access_token(ACCESS_TOKEN, ACCESS_TOKEN_SECRET)
api = tweepy.API(auth)


# In[16]:


search_terms = ['Covid19','Corona']
def stream_tweets(search_term):
    data = []
    counter = 0
    for tweet in tweepy.Cursor(api.search, q='\"{}\" -filter:retweets'.format(search_term), count=1000, lang = 'en',
                               geocode = '-6.3122791,106.6442084,100km', since ="2020-02-11", until="2020-06-19", tweet_mode='extended').items():
        tweet_details = {}
        tweet_details['name'] = tweet.user.screen_name
        tweet_details['tweet'] = tweet.full_text
        tweet_details['retweets'] = tweet.retweet_count
        tweet_details['location'] = tweet.user.location
        tweet_details['created'] = tweet.created_at.strftime("%d-%b-%y")
        tweet_details['followers'] = tweet.user.followers_count
        tweet_details['is_user_verified'] = tweet.user.verified
        data.append(tweet_details)
        
        counter += 1
        if counter == 1000:
            break
        else:
            pass
    with open('data/{}.json'.format(search_term), 'w')as f:
        json.dump(data,f)
    print('done!')


# In[17]:


if __name__ == "__main__":
    print('starting to stream....')
    for search_term in search_terms:
        stream_tweets(search_term)
    print('finished!')


# In[18]:


import pandas as pd
import numpy as np

df = pd.read_json(r'data/Covid19.json')
df.to_csv (r'data\Covid19baru.csv', index = None)


# In[19]:


df = pd.read_json(r'data/Corona.json')
df.to_csv (r'data\Coronabaru.csv', index = None)


# In[20]:


import pandas as pd
import numpy as np
import re

def clean_tweet(tweet):
    return ' '.join(re.sub('(@[A-Za-z0-9]+)|([^0-9A-Za-z \t])|(\w+:\/\/\s+)','', tweet).split())


# In[33]:


def create_covid19_df():
    covid19_df = pd.read_json('data/Covid19.json', orient='records')
    covid19_df['clean_tweet'] = covid19_df['tweet'].apply(lambda x: clean_tweet(x))
    covid19_df['clean_loc'] = covid19_df['location'].apply(lambda x: clean_tweet(x))
    covid19_df['search_name'] = 'Covid19'
    covid19_df.drop_duplicates(subset = ['name'], keep='first', inplace=True)
    return covid19_df
def create_corona_df():
    corona_df = pd.read_json('data/Corona.json', orient='records')
    corona_df['clean_tweet'] = corona_df['tweet'].apply(lambda x: clean_tweet(x))
    corona_df['clean_loc'] = corona_df['location'].apply(lambda x: clean_tweet(x))
    corona_df['search_name'] = 'Corona'
    corona_df.drop_duplicates(subset = ['name'], keep='first', inplace=True)
    return corona_df

create_corona_df()


# In[34]:


def join_dfs():
    covid_df = create_covid19_df()
    corona_df = create_corona_df()
    frames = [covid_df, corona_df]
    searchname_df = pd.concat(frames, ignore_index = True)
    return searchname_df
join_dfs()


# In[36]:


def analyze():
    searchname_df = join_dfs()
    covid19_location = create_covid19_df()
    corona_location = create_corona_df()
    
    covid_by_location = covid19_location.groupby('clean_loc')['location'].count().reset_index()
    covid_by_location.columns = ['location_name', 'numbers_of_tweets']
    
    corona_by_location = corona_location.groupby('clean_loc')['location'].count().reset_index()
    corona_by_location.columns = ['location_name','numbers_of_tweets']
    
    retweet_by_keyword = searchname_df.groupby('search_name')['retweets'].mean().reset_index()
    retweet_by_keyword.columns = ['keyword_name', 'average_of_retweets']
    
    followers_of_user_by_sn = searchname_df.groupby('search_name')['followers'].mean().reset_index()
    followers_of_user_by_sn.columns = ['keyword_name','average_no_of_followers_of_user']
    
    return (covid_by_location.sort_values(by = 'numbers_of_tweets', ascending = False).head(3),
            corona_by_location.sort_values(by = 'numbers_of_tweets', ascending = False).head(3),
            retweet_by_keyword, followers_of_user_by_sn)
analyze()


# In[37]:


from matplotlib import pyplot as plt

def plot_graphs():
    analysis_details = analyze()
    amount_covid19, amount_corona, retweet_by_searchname, average_follower = analysis_details
    
    fig1, ax1 = plt.subplots()
    ax1.bar(amount_covid19['location_name'], amount_covid19['numbers_of_tweets'], label = 'tweets by location_name')
    ax1.set_xlabel('Location')
    ax1.set_ylabel('Number of tweets')
    ax1.set_title('Amount of Covid 19 by Location')
    
    fig1, ax1 = plt.subplots()
    ax1.bar(amount_corona['location_name'], amount_corona['numbers_of_tweets'], label = 'tweets by location_name')
    ax1.set_xlabel('Location')
    ax1.set_ylabel('Number of tweets')
    ax1.set_title('Amount of Corona by Location')
    
    fig2, ax2 = plt.subplots()
    ax2.bar(retweet_by_searchname['keyword_name'], retweet_by_searchname['average_of_retweets'], label = 'tweets by keyword_name')
    ax2.set_xlabel('Keyword')
    ax2.set_ylabel('Number of retweets')
    ax2.set_title('Average of retweets by keyword')
    
    fig3, ax3 = plt.subplots()
    ax3.bar(average_follower['keyword_name'], average_follower['average_no_of_followers_of_user'], label = 'tweets by keyword_name')
    ax3.set_xlabel('Keyword')
    ax3.set_ylabel('Average')
    ax3.set_title('Average of user follower by keyword')
    
    list_of_figures = [plt.figure(i) for i in plt.get_fignums()]
    return list_of_figures
plot_graphs()


# In[38]:


def analyze():
    searchname_df = join_dfs()
    covid19_created = create_covid19_df()
    corona_created = create_corona_df()
    
    covid_by_created = covid19_created.groupby('created')['tweet'].count().reset_index()
    covid_by_created.columns = ['created', 'numbers_of_tweets']
    
    corona_by_created = corona_created.groupby('created')['tweet'].count().reset_index()
    corona_by_created.columns = ['created','numbers_of_tweets']
 
    return (covid_by_created, corona_by_created)
analyze()


# In[39]:


from matplotlib import pyplot as plt

def plot_graphs():
    analysis_details = analyze()
    covid_by, corona_by = analysis_details
    fig1, ax1 = plt.subplots(figsize=(10,3))
    ax1.plot(covid_by['created'], covid_by['numbers_of_tweets'], label = 'tweets by keyword_name')
    ax1.plot(corona_by['numbers_of_tweets'], label = 'tweets by keyword_name')
    ax1.set_ylabel('Number of tweets')
    ax1.set_xlabel('Created time')
    ax1.set_title('Amount of Tweet by Created Time')
    
    list_of_figures =[plt.figure(i) for i in plt.get_fignums()]
    return list_of_figures
plot_graphs()


# In[ ]:




